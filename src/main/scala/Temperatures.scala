import math._
import scala.util._
import scala.io.StdIn._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * */
object Temperatures extends App {
  val n = readLine.toInt
  if (n == 0) {
    println(0)
  } else {
    val temps = readLine.split(" ").map(_.toInt)
    val min = temps.map(_.abs).min
    println(temps.filter(_.abs == min).find(_ == min).getOrElse(-min))
  }
}