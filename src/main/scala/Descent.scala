import math._
import scala.util._
import scala.io.StdIn._

/**
 * The while loop represents the game.
 * Each iteration represents a turn of the game
 * where you are given inputs (the heights of the mountains)
 * and where you have to print an output (the index of the mountain to fire on)
 * The inputs you are given are automatically updated according to your last actions.
 **/
object Descent extends App {

    // game loop
    while(true) {

      println((0 until 8).map(i => (i, readLine)).maxBy(p => p._2)._1)
        
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        
//        println("4") // The index of the mountain to fire on.
    }
}