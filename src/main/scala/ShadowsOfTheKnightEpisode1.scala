import math._
import scala.util._
import scala.io.StdIn._


case class Axis(left: Int, current: Int, right: Int) {
    // binary search
    def next(dir: Int): Axis = dir match {
      case 1 => Axis(current, current + (right - current) / 2, right)
      case -1 => Axis(left, left + (current - left) / 2, current)
      case 0 => this
    }

    // no further jumps possible
    def isDead: Boolean = right - left < 1 || left == current

    // reset right bound with given value if the instance is dead
    def reset(r: Int): Axis = if(isDead) Axis(0, current, r) else this
  }

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * */
object ShadowsOfTheKnightEpisode1 extends App {
  // size of the grid
  val size = (readLine split " ").filter(_ != "").map(_.toInt)
  // number of turns
  val turns = readLine.toInt
  // s.e.
  val initialPosition = (readLine split " ").filter(_ != "").map(_.toInt)

  // convert direction to velocity
  def dir2vel(dir: String): Array[Int] = dir match {
    case "U" => Array(0, -1)
    case "UR" => Array(1, -1)
    case "R" => Array(1, 0)
    case "DR" => Array(1, 1)
    case "D" => Array(0, 1)
    case "DL" => Array(-1, 1)
    case "L" => Array(-1, 0)
    case "UL" => Array(-1, -1)
  }

  // alias for convenience
  type State = Array[Axis]

  // narrow down the search area via binary search
  def jump(s: State): State = {
    val dir = dir2vel(readLine.toUpperCase)
    s.zip(dir).map(p => p._1.next(p._2))
  }

  // display current position
  def printState(s: State): State = {
    println(s.map(_.current).mkString(" "))
    s
  }

  // reset the search area if the bomb is found (area is dead)
  def reset(s: State): State = s.zip(size).map(p => p._1.reset(p._2))

  // iteration step
  def next(s: State): State = reset(printState(jump(s)))

  val initialState = initialPosition.zip(size).map(p => Axis(0, p._1, p._2))

  (0 until turns).foldLeft(initialState)((s, _) => next(s))
}