import scala.math.pow
import System.err.{println => log}
import scala.io.StdIn.readLine

object DerivativeTime1 extends App {
    sealed trait Expr {
        def +(b: Expr): Expr = Add(this, b)
    }
    final case class Val(value: Double) extends Expr
    final case class Var(name: String)     extends Expr
    final case class Add(a: Expr, b: Expr) extends Expr
    final case class Mul(a: Expr, b: Expr) extends Expr
    final case class Pow(a: Expr, b: Expr) extends Expr

    object Expr {
        implicit class ExprOps(a: Expr) {
            def +(b: Expr): Expr = Add(a, b)

            def *(b: Expr): Expr = Mul(a, b)

            def pow(b: Expr): Expr = Pow(a, b)

            def pow(b: Double): Expr = Pow(a, Val(b))

            def pow(b: String): Expr = Pow(a, Var(b))

            def repr: String = a match {
                case Val(value) => value.toString
                case Var(name)  => name
                case Add(b, c)  => s"(${b.repr} + ${c.repr})"
                case Mul(b, c)  => s"(${b.repr} * ${c.repr})"
                case Pow(b, c)  => s"(${b.repr} ^ ${c.repr})"
            }
        }
    }

    def reorder(expr: Expr): Expr = expr match {
        case Add(Val(_), Val(_)) | Mul(Val(_), Val(_)) => expr
        case Add(a, b @ Val(_))                        => b + a
        case Mul(a, b @ Val(_))                        => b * a
        case _                                         => expr
    }

    def simplify(expr: Expr, depth: Int = 0, init: Boolean = true): Expr = {
        val prefix = ("│  " * depth) + (if (init) "├" else "└") + s"─ simplify ${expr.repr} :"
        reorder(expr) match {
            case Add(a, b) =>
                log(s"$prefix expr + expr => recognize{simplify[${a.repr}] + simplify[${b.repr}]}")
                recognize(simplify(a, depth + 1) + simplify(b, depth + 1, false), depth)
            case Mul(a, b) =>
                log(s"$prefix expr * expr => recognize{simplify[${a.repr}] * simplify[${b.repr}]}")
                recognize(simplify(a, depth + 1) * simplify(b, depth + 1, false), depth)
            case Pow(a, b) =>
                log(s"$prefix expr ^ expr => recognize{simplify[${a.repr}] ^ simplify[${b.repr}]}")
                recognize(simplify(a, depth + 1) pow simplify(b, depth + 1, false), depth)
            case e         =>
                log(s"$prefix _ => ${e.repr}")
                e
        }
    }

    def recognize(expr: Expr, depth: Int = 0): Expr = {
        val prefix = ("│  " * depth) + s"└─ recognize ${expr.repr} : "
        reorder(expr) match {
            case Add(Val(a), Val(b))               =>
                log(s"$prefix const + const => ${a + b}")
                Val(a + b)
            case Add(Val(0), b)                    =>
                log(s"$prefix 0 + expr => ${b.repr}")
                b
            case Mul(Val(0), _) | Mul(_, Val(0))   =>
                log(s"$prefix 0 * expr | expr * 0 => 0")
                Val(0)
            case Mul(Val(a), Val(b))               =>
                log(s"$prefix const * const => ${a * b}")
                Val(a * b)
            case Mul(Val(1), b)                    =>
                log(s"$prefix 1 * expr => ${b.repr}")
                b
            case Mul(Val(a), Pow(Val(b), Val(-1))) =>
                log(s"$prefix const * (const ^ -1) => ${a / b}")
                Val(a / b)
            case Pow(_, Val(0))                    =>
                log(s"$prefix expr ^ 0 => 1")
                Val(1)
            case Pow(a, Val(1))                    =>
                log(s"$prefix expr ^ 1 => ${a.repr}")
                a
            case Pow(Val(a), Val(b))               =>
                log(s"$prefix const ^ const => ${pow(a, b)}")
                Val(pow(a, b))
            case Add(Val(a), Add(Val(b), c))       =>
                log(s"$prefix const + (const + expr) => ($a + $b) + ${c.repr}")
                Val(a + b) + c
            case Mul(Val(a), Mul(Val(b), c))       =>
                log(s"$prefix const * (const * expr) => ($a * $b) * ${c.repr}")
                Val(a * b) * c
            case e                                 =>
                log(s"$prefix _ => ${e.repr}")
                e
        }
    }

    def derive(expr: Expr, respect: String): Expr = reorder(expr) match {
        case Val(_)                          => Val(0)
        case Var(`respect`)                  => Val(1)
        case Var(_)                          => Val(0)
        case Pow(a @ Var(`respect`), Val(2)) => Val(2) * a
        case Pow(a @ Var(`respect`), Val(b)) => Val(b) * a.pow(b - 1)
        case Pow(a, b)                       => b * (a.pow(b + Val(-1)) * derive(a, respect))
        case Add(Val(_), Val(_))             => Val(0)
        case Add(Val(_), b)                  => derive(b, respect)
        case Add(a, b)                       => derive(a, respect) + derive(b, respect)
        case Mul(a @ Val(_), Var(`respect`)) => a
        case Mul(a, b)                       => derive(a, respect) * b + a * derive(b, respect)
    }

    def derive(expr: Expr, respects: Seq[String]): Expr = respects.foldLeft(expr)((e, r) => simplify(derive(e, r)))

    def substitute(expr: Expr, values: Map[String, Double]): Expr = expr match {
        case Var(name) => Val(values(name))
        case Add(a, b) => substitute(a, values) + substitute(b, values)
        case Mul(a, b) => substitute(a, values) * substitute(b, values)
        case Pow(a, b) => substitute(a, values) pow substitute(b, values)
        case _         => expr
    }

    val valRegex = "(-?\\d+(?:\\.\\d+)?).*".r
    val varRegex = "([a-zA-Z]\\w*).*".r

    def parseOperand(s: String): (Expr, Int) = {
        log(s"parsing operand '$s'")
        s match {
            case valRegex(value) =>
                log(s"matched a Val($value) #${value.length}")
                (Val(value.toDouble), value.length)
            case varRegex(name)  =>
                log(s"matched a Var($name) #${name.length}")
                (Var(name), name.length)
            case _               =>
                log(s"matched neither Val nor Var, trying to parse as operation")
                parseOperation(s.drop(1))
        }
    }

    def parseOperation(s: String): (Expr, Int) = {
        log(s"parsing operation '$s'")
        val (a, lengthA) = parseOperand(s)
        val operator     = s(lengthA)
        val (b, lengthB) = parseOperand(s.drop(lengthA + 1))

        log(s"parsed operation $s into '${a.repr} [$operator] ${b.repr}'")

        // +3 = 1 for opening bracket +  1 for operator + 1 for closing bracket

        log(s"a.length: $lengthA | b.length: $lengthB | total length: ${lengthA + lengthB + 3}")
        (
          operator match {
              case '+' => a + b
              case '-' => a + (Val(-1) * b)
              case '*' => a * b
              case '/' => a * b.pow(-1)
              case '^' => a pow b
          },
          lengthA + 3 + lengthB
        )
    }

    def parse(s: String): Expr =
        parseOperand(s)._1

    def parseValues(values: String): Map[String, Double] = {
        val tokens = values.split(' ')
        (tokens.indices by 2).map(i => (tokens(i), tokens(i + 1).toDouble)).toMap
    }

    def process(expression: String, respects: String, values: String): Double = {
        log(s"\nParsed values: ${parseValues(values)}\n")

        simplify(substitute(derive(parse(expression), respects.split(' ')), parseValues(values))) match {
            case Val(a) => a
            case _      => Double.NaN
        }
    }

    println(process(readLine, readLine, readLine).round)
}
