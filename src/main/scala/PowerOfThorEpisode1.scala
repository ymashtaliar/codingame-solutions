import math._
import scala.util._
import scala.io.StdIn._


object PowerOfThorEpisode1 extends App {
    val Array(lightX, lightY, initialTx, initialTy) = (readLine split " ").filter(_ != "").map(_.toInt)

    // translate velocity to direction
    def vel2dir(vx: Int, vy: Int): String = {
        (vy match {
            case 1  => "S"
            case -1 => "N"
            case _  => ""
        }) + (vx match {
            case 1  => "E"
            case -1 => "W"
            case _  => ""
        })
    }

    // round to precision
    def roundUp(x: Double, p: Int): Double = {
        (x * pow(10, p)).ceil / pow(10, p)
    }

    // distance to overcome along Ox and Oy
    val dx = lightX - initialTx
    val dy = lightY - initialTy
    // velocity, i.e. values to add to the player position each step
    val vx: Double = if (dx.abs > dy.abs) dx.sign else roundUp(dx.toDouble / dy.abs, 4)
    val vy: Double = if (dy.abs > dx.abs) dy.sign else roundUp(dy.toDouble / dx.abs, 4)

    // alias for convenience
    type Position = (Double, Double)

    // update position with velocity
    def step(pos: Position): Position = (pos._1 + vx, pos._2 + vy)

    // move the character (truncating each coordinate)
    def printDirection(pos: Position): Position = {
        println(vel2dir(pos._1.toInt, pos._2.toInt))
        pos
    }

    // ensure each coordinate to be within (-1; 1)
    def normalize(pos: Position): Position = (pos._1 % 1, pos._2 % 1)

    // iteration
    def next(pos: Position): Position = {
        val update = normalize(printDirection(step(pos)))
        readLine
        update
    }

    val remainingTurns = readLine.toInt
    val initialPos: (Double, Double) = (0, 0)
    (0 until remainingTurns).foldLeft(initialPos)((pos, _) => next(pos))
}
