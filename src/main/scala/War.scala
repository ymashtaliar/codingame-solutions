import scala.annotation.tailrec
import scala.io.StdIn._

object War extends App {
    // convert card value to int
    def order(card: String): Int = {
        card.dropRight(1) match {
            case "A" => 14
            case "K" => 13
            case "Q" => 12
            case "J" => 11
            case n   => n.toInt
        }
    }

    // read decks
    val q1 = (0 until readLine.toInt).map(_ => order(readLine))
    val q2 = (0 until readLine.toInt).map(_ => order(readLine))

    case class State(a: Seq[Int], b: Seq[Int], rounds: Int, end: Boolean = false) {
        // one game turn
        def next: State = {
            if (a.isEmpty || b.isEmpty) {
                this.copy(end = true)
            } else {
                a.head compare b.head match {
                    case x if x > 0 => State(a.drop(1) :+ a.head :+ b.head, b.drop(1), rounds + 1)
                    case x if x < 0 => State(a.drop(1), b.drop(1) :+ a.head :+ b.head, rounds + 1)
                    case 0          => war()
                }
            }
        }

        // offset for chained wars
        def war(offset: Int = 4): State = {
            if (a.size-offset < 1 || b.size-offset < 1) {
                this.copy(end = true)
            } else {
                a(offset) compare b(offset) match {
                    case x if x > 0 => State(a.drop(offset+1) ++ a.take(offset+1) ++ b.take(offset+1), b.drop(offset+1), rounds + 1)
                    case x if x < 0 => State(a.drop(offset+1), b.drop(offset+1) ++ a.take(offset+1) ++ b.take(offset+1), rounds + 1)
                    case 0          => war(offset+4)
                }
            }
        }

        // neither of players run out of cards
        def pat: Boolean = a.size * b.size != 0

        def winner: Int = if (b.isEmpty) 1 else 2
    }

    @tailrec def applyWhile[A](x: A, predicate: A => Boolean, f: A => A): A = {
        if (predicate(x)) applyWhile(f(x), predicate, f) else x
    }

    val s = applyWhile[State](State(q1, q2, 0), !_.end, _.next)
    println(if (s.pat) "PAT" else s"${s.winner} ${s.rounds}")
}
